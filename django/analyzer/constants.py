HERO = "Hero"

PREFLOP = "Preflop"
FLOP = "Flop"
TURN = "Turn"
RIVER = "River"

CHECK = "Check"
BET = "Bet"
CALL = "Call"
RAISE = "Raise"
FOLD = "Fold"
POST = "Post"
ANTE = "Ante"

PLO8 = "PLO8"
PLO = "PLO"
NL = "NL"

CASH = "Cash"
STT = "STT"
MTT = "MTT"

STREETS = [PREFLOP, FLOP, TURN, RIVER]
ACTIONS = [CHECK, BET, RAISE, FOLD, POST, ANTE]
GAME = [PLO, NL]
FORMAT = [CASH, STT, MTT]
