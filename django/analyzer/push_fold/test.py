from django.test import TestCase
from pathlib import Path
from analyzer.push_fold.ranges import RangeHandler, format_range
from analyzer.push_fold.report import find_hands, score_hands
from analyzer.parser.main import MainParser

TEST_DIR = Path(__file__).resolve().parent.joinpath("test_files")


class PushFoldTest(TestCase):
    def test_filter(self):
        pp = MainParser(TEST_DIR)
        sessions = pp.parse()
        ai = find_hands(sessions[0])


    def test_score(self):
        pp = MainParser(TEST_DIR)
        sessions = pp.parse()
        ai = find_hands(sessions[0])
        r = score_hands(ai)

        for k in r.keys():
            if len(r[k].all_hands) > 10:
                print(r[k])

    def test_ranges(self):
        rh = RangeHandler()
        r = rh.get_range(15, 3)
        self.assertEqual(['QJo', 'A3s', 'K8s', 'A7o', 'A4s', 'QTs', 'KTo', 'A8o', 'A6s', 'A5s', 'K9s', 'QJs', '55', 'KJo', 'A9o', 'A7s', 'KQo', 'KTs', 'A8s', 'KJs', 'ATo', 'A9s', '66', 'KQs', 'AJo', 'AQo', 'ATs', 'AKo', 'AJs', 'AQs', '77', 'AKs', '88', '99', 'TT', 'JJ', 'QQ', 'KK', 'AA'], r)

    def test_format(self):
        values = [
            "K3o",
            "J8o",
            "T9o",
            "Q7o",
            "Q4s",
            "J7s",
            "K4o",
            "T8s",
            "Q5s",
            "K2s",
            "J9o",
            "K5o",
            "Q8o",
            "Q6s",
            "33",
            "J8s",
            "T9s",
            "K3s",
            "K6o",
            "Q7s",
            "K4s",
            "A2o",
            "K7o",
            "JTo",
            "Q9o",
            "J9s",
            "K5s",
            "A3o",
            "Q8s",
            "K8o",
            "K6s",
            "A4o",
            "44",
            "QTo",
            "A2s",
            "JTs",
            "K7s",
            "Q9s",
            "A6o",
            "A5o",
            "K9o",
            "QJo",
            "A3s",
            "K8s",
            "A7o",
            "A4s",
            "QTs",
            "KTo",
            "A8o",
            "A6s",
            "A5s",
            "K9s",
            "QJs",
            "55",
            "KJo",
            "A9o",
            "A7s",
            "KQo",
            "KTs",
            "A8s",
            "KJs",
            "ATo",
            "A9s",
            "66",
            "KQs",
            "AJo",
            "AQo",
            "ATs",
            "AKo",
            "AJs",
            "AQs",
            "77",
            "AKs",
            "88",
            "99",
            "TT",
            "JJ",
            "QQ",
            "KK",
            "AA",
        ]
        f = format_range(values)
        self.assertEqual("K3o+,J8o+,T9o,Q7o+,Q4s+,J7s+,T8s+,K2s+,33+,A2o+,A2s+", f)