from analyzer.parser.parser_data import Game, Session, Hand, PlayerHand, Action
from analyzer.constants import *
from .parser_utils import *
import re


def parse_street(street, lines, hand):
    for line in lines:
        p = parse_player(line, hand)
        if p is None:
            continue

        if "esult" in line:
            m = parse_money(line)
            p.amount_won += m
        elif line.endswith(": Checks"):
            action = Action(p.name, street, CHECK, 0)
            hand.actions.append(action)
        elif ": Folds" in line:
            action = Action(p.name, street, FOLD, 0)
            hand.actions.append(action)
        elif ": All-in" in line:
            m = parse_money(line)

            act = RAISE if "(raise)" in line else BET

            if act == BET:
                for action in hand.actions:
                    if action.street == street:
                        if action.action == BET or action.action == RAISE:
                            act = CALL
                            break

            action = Action(p.name, street, act, m)
            hand.actions.append(action)
        elif ": Bets " in line:
            m = parse_money(line)
            action = Action(p.name, street, BET, m)
            hand.actions.append(action)
        elif ": Calls " in line:
            m = parse_money(line)
            action = Action(p.name, street, CALL, m)
            hand.actions.append(action)
        elif ": Raises " in line:
            m = parse_money(line)
            action = Action(p.name, street, RAISE, m)
            hand.actions.append(action)
        elif "Return uncalled portion of bet" in line:
            m = parse_money(line)
            p.uncalledBets = m
        elif "Ranking" in line:
            parts = line.split(" ")
            p.tournamentFinish = int(parts[len(parts) - 1])
        elif "Prize Cash" in line:
            p.tournamentWinnings = parse_money(line)
        elif "Does not show" in line or "Mucks" in line or "Showdown" in line:
            pass
            """ Fix this someday
            if "[" in line:
                pos = line.index("[")
                if "[" in line[pos + 1:]:
                    pos = line.index("[", pos + 1)

                f = line[pos:]
                p.finalHand = f

            else:
                print(line)
            """

