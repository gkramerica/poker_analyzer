from analyzer.constants import *


class PlayerHand:
    def __init__(self, name, pos):
        self.name = name
        self.pos = pos
        self.starting_stack = 0.0
        self.amount_won = 0.0
        self.uncalled_bets = 0.0
        self.cards = []
        self.final_hand = ""
        self.rake = 0
        self.tournament_finish = 0
        self.tournament_winnings = 0

    def format_hand(self):
        c1 = self.cards[0]
        c2 = self.cards[1]

        h = c1[0:1] + c2[0:1]
        if c1[0:1] != c2[0:1]:
            if c1[1:2] == c2[1:2]:
                h += "s"
            else:
                h += "o"

        return h


    def __str__(self):
        return f"{self.name}"


class Game:
    def __init__(self, file_name):
        self.file_name = file_name
        self.game_format = CASH
        self.game_type = NL
        self.buyin = None
        self.tournament_buyin = None

    def __str__(self):
        if self.game_format == CASH:
            return f"{self.game_format} {self.game_type} {self.buyin}"
        else:
            return f"{self.game_format} {self.game_type} {self.tournament_buyin}"


class Session:
    def __init__(self, game):
        self.game = game
        self.hands = []

    def __str__(self):
        return f"{self.game} {len(self.hands)} hands"


class Action:
    def __init__(self, player, street, action, amount):
        self.player = player
        self.street = street
        self.action = action
        self.amount = amount

    def __str__(self):
        return f"{self.player} {self.street} {self.action} {self.amount}"

class Hand:
    def __init__(self, num, table, timestamp):
        self.timestamp = timestamp
        self.table = table
        self.num = num
        self.small_blind = 0
        self.big_blind = 0
        self.ante = 0
        self.players = []
        self.actions = []
        self.board_cards = []

    def add_player(self, name, pos):
        p = PlayerHand(name, pos)
        self.players.append(p)
        return p

    def get_player(self, name):
        for p in self.players:
            if p.name == name:
                return p

        return None

    def get_total_bet(self, player_name):
        bet = 0

        for p in self.players:
            if p.name == player_name:
                for action in self.actions:
                    if action.player == player_name:
                        bet += action.amount

        return 0

    def find_player(self, pos):
        for p in self.players:
            if p.pos == pos:
                return p

        return None

    def __str__(self):
        return f"{self.num}: table={self.table}, {self.timestamp}, {len(self.players)} players, cards={self.board_cards}"

