import sys
import os

sys.path.append(os.path.join("django"))


from analyzer.push_fold.ranges import RangeHandler, format_range
from analyzer.push_fold.report import find_hands, score_hands
from analyzer.parser.main import MainParser
from analyzer.constants import *

class PushStats():
    def __init__(self, name):
        self.name = name
        self.total=0
        self.close=0
        self.shoves=0
        self.push_mistakes=0
        self.fold_mistakes=0

    def add_hand(self, r):
        self.total += len(r.all_hands)
        self.close += len(r.close_hands)
        self.shoves += len(r.shoves)
        self.push_mistakes += len(r.push_mistakes)
        self.fold_mistakes += len(r.fold_mistakes)

    def print_totals(self):
        pct = (self.push_mistakes + self.fold_mistakes) / self.total
        pct_shove = round((self.shoves) / self.total * 100)
        pct_fold = (self.fold_mistakes) / self.total
        pct_push = (self.push_mistakes) / self.total
        print(f"{self.name}\t{self.total}\t{self.close}\t{pct_shove}\t{round(pct_fold* 100)}\t{round(pct_push*100)}\t{round(pct * 100)}")

    def __str__(self):
        return f"{self.name}\t{self.total}"

groups = {}

pp = MainParser(os.path.join("hand_history"))
sessions = pp.parse()
for s in sessions:
    if s.game.game_format == MTT and s.game.game_type == NL:
        ai = find_hands(s)
        r = score_hands(ai, 5)
        for k in r.keys():
            if k.startswith(HERO):
                for h in r[k].fold_mistakes:
                    print(h)
                for h in r[k].push_mistakes:
                    print(h)

            if k not in groups:
                groups[k] = PushStats(k)

            groups[k].add_hand(r[k])

gg = list(groups.keys())
gg.sort()
for k in gg:
    groups[k].print_totals()
"""
        for k in r.keys():
           if len(r[k].all_hands) > 10:
                print(r[k])
                """