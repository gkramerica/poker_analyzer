def card_sort_value(c):
    rank = c[0:1]
    if rank == 'A':
        rank = '14'
    elif rank == 'K':
        rank = '13'
    elif rank == 'Q':
        rank = '12'
    elif rank == 'J':
        rank = '11'
    elif rank == 'T':
        rank = '10'

    v = int(rank) * 10

    suits = {'c': 0, 'd': 1, 'h': 2, 's': 3}

    v += suits[c[1:2]]

    return -v

def get_position_name(pos, num_players):
    p = ['Big Blind', 'Small Blind', 'Button', 'CO', 'HJ', 'LJ']

    x = pos + 2

    if x >= len(p):
        return 'LJ+' + str(x - len(p) + 1)
    else:
        return p[x]