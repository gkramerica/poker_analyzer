import re
import math
from analyzer.constants import *


def compute_buy_in(game, hands):
    if game.game_format == CASH:
        bb = 0
        for hand in hands:
            if hand.big_blind > bb:
                game.buyin = round(hand.big_blind * 100)
                bb = hand.big_blind


def parse_money(line):
    if "$" in line:
        pos = line.index("$")
        line = line[pos + 1 :]
        if " " in line:
            pos2 = line.index(" ")
        else:
            if "]" in line:
                pos2 = line.index("]")
            else:
                return float(line)

        s = line[:pos2]
        return float(s)

    # Tournaments do not use $ sign
    line = line.replace(")", " ")
    line = line.replace("(", " ").strip()

    parts = line.split(" ")

    if len(parts) < 2:
        return 0

    index = len(parts) - 1

    if line.endswith("in chips"):
        index = index - 2
    elif parts[index - 1] == "to":
        index = index - 2

    value = parts[index].replace(",", "")
    value = value.replace("$", "")

    try:
        v = float(value)
    except:
        print(f"failed to parse number: {value} in {line}")
        return 0

    if math.isnan(v):
        return 0

    return v


def parse_player(line, hand):
    if ":" in line:
        pos = line.index(":")
        s = line[0 : pos - 1]
        s = s.replace("[ME]", "")
        s = s.strip()

        return hand.find_player(s)

    return None


def find_regex(regex, s):
    f = re.findall(regex, s)
    if len(f) > 0:
        return f[0]

    return None


def fix_up_hands(game, hands):
    for hand in hands:
        fix_up_hand(game, hand)


def fix_up_hand(game, hand):
    totalPot = 0
    totalBet = 0

    winners = []

    for player in hand.players:
        if player.pos == "Big Blind":
            player.pos = -2
        elif player.pos == "Small Blind":
            player.pos = -1
        elif player.pos == "Dealer":
            player.pos = 0
        else:
            offset = 3
            if "+" in player.pos:
                u = player.pos[len(player.pos) - 1 :]
                offset = 3 + int(u)

            player.pos = len(hand.players) - offset

        b = hand.get_total_bet(player.name)

        totalBet += b

        if player.amount_won > 0:
            winners.append(player)
            totalPot += player.amount_won

    if game.game_type == CASH:
        totalRake = totalBet - totalPot

        if totalRake > 0:
            if winners.length == 1:
                winners[0].rake = totalRake
            else:
                for winner in winners:
                    ratio = winner.amount_won / totalPot
                    r = totalRake * ratio
                    winner.rake = r

                    totalRake = totalRake - r



