Ignition Hand #3699425355: HOLDEM Tournament #22854969 TBL#1, Turbo- Level 1 (10/20) - 2018-06-26 18:16:11
Seat 5: Big Blind (500 in chips)
Seat 2: UTG (500 in chips)
Seat 3: UTG+1 (500 in chips)
Seat 4: UTG+2 (500 in chips)
Seat 1: Dealer [ME] (500 in chips)
Seat 6: Small Blind (500 in chips)
Dealer [ME] : Table enter user
Big Blind : Table enter user
UTG : Table enter user
UTG+2 : Table enter user
Big Blind : Draw for dealer [2s] 
UTG : Draw for dealer [8s] 
UTG+1 : Draw for dealer [7s] 
UTG+2 : Draw for dealer [9c] 
Dealer [ME] : Draw for dealer [Ad] 
Small Blind : Draw for dealer [Jh] 
Dealer [ME] : Set dealer [5] 
Small Blind : Small blind 10 
Big Blind : Big blind 20 
*** HOLE CARDS ***
Big Blind : Card dealt to a spot [Js 3d] 
UTG : Card dealt to a spot [Kc Tc] 
UTG+1 : Card dealt to a spot [Ks Jh] 
UTG+2 : Card dealt to a spot [Jc Kh] 
Dealer [ME] : Card dealt to a spot [Td As] 
Small Blind : Card dealt to a spot [3s 2c] 
Small Blind : Table enter user
UTG : Raises 40 to 40
UTG+1 : Fold(Blind Disconnected)
UTG+2 : Call 40 
Dealer [ME] : Raises 155 to 155
Small Blind : Folds
Big Blind : Folds
UTG : Call 115 
UTG+2 : Folds
*** FLOP *** [5h 2d 4d]
UTG : Checks
Dealer [ME] : All-in 345 
UTG : Folds
Dealer [ME] : Return uncalled portion of bet 345 
Dealer [ME] : Does not show [Td As] (High Card)
Dealer [ME] : Hand Result 380 
*** SUMMARY ***
Total Pot(380)
Board [5h 2d 4d  ]
Seat+5: Big Blind Folded on the FLOP
Seat+2: UTG Folded on the FLOP
Seat+3: UTG+1 Folded on the FLOP
Seat+4: UTG+2 Folded on the FLOP
Seat+1: Dealer 380 [Does not show]  
Seat+6: Small Blind Folded on the FLOP


Ignition Hand #3699425831: HOLDEM Tournament #22854969 TBL#1, Turbo- Level 1 (10/20) - 2018-06-26 18:16:53
Seat 5: Small Blind (480 in chips)
Seat 2: Big Blind (345 in chips)
Seat 3: UTG (500 in chips)
Seat 4: UTG+1 (460 in chips)
Seat 1: UTG+2 [ME] (725 in chips)
Seat 6: Dealer (490 in chips)
Dealer : Set dealer [6] 
Small Blind : Small blind 10 
Big Blind : Big blind 20 
*** HOLE CARDS ***
Small Blind : Card dealt to a spot [9h 5d] 
Big Blind : Card dealt to a spot [Td Qs] 
UTG : Card dealt to a spot [Th As] 
UTG+1 : Card dealt to a spot [6h 9s] 
UTG+2 [ME] : Card dealt to a spot [Kh 5s] 
Dealer : Card dealt to a spot [Ks Js] 
UTG : Fold(Blind Disconnected)
UTG+1 : Folds
UTG+2 [ME] : Folds
Dealer : Call 20 
UTG : Table enter user
Small Blind : Call 10 
Big Blind : Checks
*** FLOP *** [4d 5c 3c]
Small Blind : Bets 40 
Big Blind : Folds
Dealer : Folds
Small Blind : Return uncalled portion of bet 40 
Small Blind : Does not show [9h 5d] (One pair)
Small Blind : Hand Result 60 
*** SUMMARY ***
Total Pot(60)
Board [4d 5c 3c  ]
Seat+5: Small Blind 60 [Does not show]  
Seat+2: Big Blind Folded on the FLOP
Seat+3: UTG Folded on the FLOP
Seat+4: UTG+1 Folded on the FLOP
Seat+1: UTG+2 Folded on the FLOP
Seat+6: Dealer Folded on the FLOP


Ignition Hand #3699426129: HOLDEM Tournament #22854969 TBL#1, Turbo- Level 1 (10/20) - 2018-06-26 18:17:35
Seat 5: Dealer (520 in chips)
Seat 2: Small Blind (325 in chips)
Seat 3: Big Blind (500 in chips)
Seat 4: UTG (460 in chips)
Seat 1: UTG+1 [ME] (725 in chips)
Seat 6: UTG+2 (470 in chips)
Dealer : Set dealer [1] 
Small Blind : Small blind 10 
Big Blind : Big blind 20 
*** HOLE CARDS ***
Dealer : Card dealt to a spot [8s 7s] 
Small Blind : Card dealt to a spot [7c 2s] 
Big Blind : Card dealt to a spot [As 4h] 
UTG : Card dealt to a spot [7d Qc] 
UTG+1 [ME] : Card dealt to a spot [Qd 4s] 
UTG+2 : Card dealt to a spot [9h Jc] 
UTG : Folds
UTG+1 [ME] : Folds
UTG+2 : Folds
Dealer : Call 20 
Small Blind : Folds
Big Blind : Checks
*** FLOP *** [Td 6c 3d]
Big Blind : Checks
Dealer : Checks
*** TURN *** [Td 6c 3d] [Jd]
Big Blind : Checks
Dealer : Checks
*** RIVER *** [Td 6c 3d Jd] [Kc]
Big Blind : Checks
Dealer : Bets 50 
Big Blind : Folds
Dealer : Return uncalled portion of bet 50 
Dealer : Does not show [8s 7s] (High Card)
Dealer : Hand Result 50 
*** SUMMARY ***
Total Pot(50)
Board [Td 6c 3d Jd Kc]
Seat+5: Dealer 50 [Does not show]  
Seat+2: Small Blind Folded on the FLOP
Seat+3: Big Blind Folded on the RIVER
Seat+4: UTG Folded on the FLOP
Seat+1: UTG+1 Folded on the FLOP
Seat+6: UTG+2 Folded on the FLOP


Ignition Hand #3699426507: HOLDEM Tournament #22854969 TBL#1, Turbo- Level 1 (10/20) - 2018-06-26 18:18:29
Seat 5: UTG+2 (550 in chips)
Seat 2: Dealer (315 in chips)
Seat 3: Small Blind (480 in chips)
Seat 4: Big Blind (460 in chips)
Seat 1: UTG [ME] (725 in chips)
Seat 6: UTG+1 (470 in chips)
Dealer : Set dealer [2] 
Small Blind : Small blind 10 
Big Blind : Big blind 20 
*** HOLE CARDS ***
UTG+2 : Card dealt to a spot [6c Js] 
Dealer : Card dealt to a spot [8h 4h] 
Small Blind : Card dealt to a spot [2s 5h] 
Big Blind : Card dealt to a spot [5s 7h] 
UTG [ME] : Card dealt to a spot [Tc Jd] 
UTG+1 : Card dealt to a spot [Qh Ac] 
UTG [ME] : Folds
UTG+1 : Raises 40 to 40
UTG+2 : Folds
Dealer : Folds
Small Blind : Folds
Big Blind : Call 20 
*** FLOP *** [7s 6h 3c]
Big Blind : Checks
UTG+1 : Bets 45 
Big Blind : Call 45 
*** TURN *** [7s 6h 3c] [Ah]
Big Blind : Checks
UTG+1 : Bets 90 
Big Blind : Folds
UTG+1 : Return uncalled portion of bet 90 
UTG+1 : Does not show [Qh Ac] (One pair)
UTG+1 : Hand Result 180 
*** SUMMARY ***
Total Pot(180)
Board [7s 6h 3c Ah ]
Seat+5: UTG+2 Folded on the FLOP
Seat+2: Dealer Folded on the FLOP
Seat+3: Small Blind Folded on the FLOP
Seat+4: Big Blind Folded on the TURN
Seat+1: UTG Folded on the FLOP
Seat+6: UTG+1 180 [Does not show]  


Ignition Hand #3699426801: HOLDEM Tournament #22854969 TBL#1, Turbo- Level 2 (15/30) - 2018-06-26 18:19:11
Seat 5: UTG+1 (550 in chips)
Seat 2: UTG+2 (315 in chips)
Seat 3: Dealer (470 in chips)
Seat 4: Small Blind (375 in chips)
Seat 1: Big Blind [ME] (725 in chips)
Seat 6: UTG (565 in chips)
Dealer : Set dealer [3] 
Small Blind : Small blind 15 
Big Blind [ME] : Big blind 30 
*** HOLE CARDS ***
UTG+1 : Card dealt to a spot [Qh 6s] 
UTG+2 : Card dealt to a spot [Td 2d] 
Dealer : Card dealt to a spot [6c 3c] 
Small Blind : Card dealt to a spot [6h 5d] 
Big Blind [ME] : Card dealt to a spot [7c 4d] 
UTG : Card dealt to a spot [3h 7h] 
UTG : Folds
UTG+1 : Folds
UTG+2 : Folds
Dealer : Raises 60 to 60
Small Blind : Folds
Big Blind [ME] : Folds
Dealer : Return uncalled portion of bet 30 
Dealer : Does not show [6c 3c] (High Card)
Dealer : Hand Result 75 
*** SUMMARY ***
Total Pot(75)
Seat+5: UTG+1 Folded on the FLOP
Seat+2: UTG+2 Folded on the FLOP
Seat+3: Dealer 75 [Does not show]  
Seat+4: Small Blind Folded on the FLOP
Seat+1: Big Blind Folded on the FLOP
Seat+6: UTG Folded on the FLOP


Ignition Hand #3699426997: HOLDEM Tournament #22854969 TBL#1, Turbo- Level 2 (15/30) - 2018-06-26 18:19:38
Seat 5: UTG (550 in chips)
Seat 2: UTG+1 (315 in chips)
Seat 3: UTG+2 (515 in chips)
Seat 4: Dealer (360 in chips)
Seat 1: Small Blind [ME] (695 in chips)
Seat 6: Big Blind (565 in chips)
Dealer : Set dealer [4] 
Small Blind [ME] : Small blind 15 
Big Blind : Big blind 30 
*** HOLE CARDS ***
UTG : Card dealt to a spot [8h 3d] 
UTG+1 : Card dealt to a spot [4s Jh] 
UTG+2 : Card dealt to a spot [6c Td] 
Dealer : Card dealt to a spot [6d 6s] 
Small Blind [ME] : Card dealt to a spot [8c 3c] 
Big Blind : Card dealt to a spot [3h 4d] 
UTG : Folds
UTG+1 : Folds
UTG+2 : Sit out
UTG+2 : Folds(timeout)
Dealer : Raises 60 to 60
Small Blind [ME] : Folds
Big Blind : Folds
Dealer : Return uncalled portion of bet 30 
Dealer : Does not show [6d 6s] (High Card)
Dealer : Hand Result 75 
*** SUMMARY ***
Total Pot(75)
Seat+5: UTG Folded on the FLOP
Seat+2: UTG+1 Folded on the FLOP
Seat+3: UTG+2 Folded on the FLOP
Seat+4: Dealer 75 [Does not show]  
Seat+1: Small Blind Folded on the FLOP
Seat+6: Big Blind Folded on the FLOP


Ignition Hand #3699427245: HOLDEM Tournament #22854969 TBL#1, Turbo- Level 2 (15/30) - 2018-06-26 18:20:11
Seat 5: Big Blind (550 in chips)
Seat 2: UTG (315 in chips)
Seat 3: UTG+1 (515 in chips)
Seat 4: UTG+2 (405 in chips)
Seat 1: Dealer [ME] (680 in chips)
Seat 6: Small Blind (535 in chips)
Dealer [ME] : Set dealer [5] 
Small Blind : Small blind 15 
Big Blind : Big blind 30 
*** HOLE CARDS ***
Big Blind : Card dealt to a spot [3d Jh] 
UTG : Card dealt to a spot [Kc 2d] 
UTG+1 : Card dealt to a spot [3h 8h] 
UTG+2 : Card dealt to a spot [Ah Qc] 
Dealer [ME] : Card dealt to a spot [4s 8d] 
Small Blind : Card dealt to a spot [8c Tc] 
UTG : Folds
UTG+1 : Folds
UTG+2 : Raises 71 to 71
Dealer [ME] : Folds
UTG+1 : Re-join
Small Blind : Folds
Big Blind : Folds
UTG+2 : Return uncalled portion of bet 41 
UTG+2 : Does not show [Ah Qc] (High Card)
UTG+2 : Hand Result 75 
*** SUMMARY ***
Total Pot(75)
Seat+5: Big Blind Folded on the FLOP
Seat+2: UTG Folded on the FLOP
Seat+3: UTG+1 Folded on the FLOP
Seat+4: UTG+2 75 [Does not show]  
Seat+1: Dealer Folded on the FLOP
Seat+6: Small Blind Folded on the FLOP


Ignition Hand #3699427402: HOLDEM Tournament #22854969 TBL#1, Turbo- Level 2 (15/30) - 2018-06-26 18:20:32
Seat 5: Small Blind (520 in chips)
Seat 2: Big Blind (315 in chips)
Seat 3: UTG (515 in chips)
Seat 4: UTG+1 (450 in chips)
Seat 1: UTG+2 [ME] (680 in chips)
Seat 6: Dealer (520 in chips)
Dealer : Set dealer [6] 
Small Blind : Small blind 15 
Big Blind : Big blind 30 
*** HOLE CARDS ***
Small Blind : Card dealt to a spot [2d 6c] 
Big Blind : Card dealt to a spot [5s 3c] 
UTG : Card dealt to a spot [8c 3d] 
UTG+1 : Card dealt to a spot [4d Qc] 
UTG+2 [ME] : Card dealt to a spot [3h Kd] 
Dealer : Card dealt to a spot [Ah Kh] 
UTG : Folds
UTG+1 : Folds
UTG+2 [ME] : Folds
Dealer : Raises 60 to 60
Small Blind : Folds
Big Blind : Call 30 
*** FLOP *** [Ks 4s Qd]
Big Blind : Checks
Dealer : Bets 67 
Big Blind : Folds
Dealer : Return uncalled portion of bet 67 
Dealer : Does not show [Ah Kh] (One pair)
Dealer : Hand Result 135 
*** SUMMARY ***
Total Pot(135)
Board [Ks 4s Qd  ]
Seat+5: Small Blind Folded on the FLOP
Seat+2: Big Blind Folded on the FLOP
Seat+3: UTG Folded on the FLOP
Seat+4: UTG+1 Folded on the FLOP
Seat+1: UTG+2 Folded on the FLOP
Seat+6: Dealer 135 [Does not show]  


Ignition Hand #3699427625: HOLDEM Tournament #22854969 TBL#1, Turbo- Level 2 (15/30) - 2018-06-26 18:21:02
Seat 5: Dealer (505 in chips)
Seat 2: Small Blind (255 in chips)
Seat 3: Big Blind (515 in chips)
Seat 4: UTG (450 in chips)
Seat 1: UTG+1 [ME] (680 in chips)
Seat 6: UTG+2 (595 in chips)
Dealer : Set dealer [1] 
Small Blind : Small blind 15 
Big Blind : Big blind 30 
*** HOLE CARDS ***
Dealer : Card dealt to a spot [6d Kd] 
Small Blind : Card dealt to a spot [2s 7c] 
Big Blind : Card dealt to a spot [8s 3h] 
UTG : Card dealt to a spot [6c 7d] 
UTG+1 [ME] : Card dealt to a spot [Qh Ah] 
UTG+2 : Card dealt to a spot [9d 4s] 
UTG : Folds
UTG+1 [ME] : Raises 90 to 90
UTG+2 : Folds
Dealer : Folds
Small Blind : Folds
Big Blind : Folds
UTG+1 [ME] : Return uncalled portion of bet 60 
UTG+1 [ME] : Does not show [Qh Ah] (High Card)
UTG+1 [ME] : Hand Result 75 
*** SUMMARY ***
Total Pot(75)
Seat+5: Dealer Folded on the FLOP
Seat+2: Small Blind Folded on the FLOP
Seat+3: Big Blind Folded on the FLOP
Seat+4: UTG Folded on the FLOP
Seat+1: UTG+1 75 [Does not show]  
Seat+6: UTG+2 Folded on the FLOP


Ignition Hand #3699427859: HOLDEM Tournament #22854969 TBL#1, Turbo- Level 2 (15/30) - 2018-06-26 18:21:35
Seat 5: UTG+2 (505 in chips)
Seat 2: Dealer (240 in chips)
Seat 3: Small Blind (485 in chips)
Seat 4: Big Blind (450 in chips)
Seat 1: UTG [ME] (725 in chips)
Seat 6: UTG+1 (595 in chips)
Dealer : Set dealer [2] 
Small Blind : Small blind 15 
Big Blind : Big blind 30 
*** HOLE CARDS ***
UTG+2 : Card dealt to a spot [Qc 2d] 
Dealer : Card dealt to a spot [Qs Th] 
Small Blind : Card dealt to a spot [9s 9h] 
Big Blind : Card dealt to a spot [4c 7s] 
UTG [ME] : Card dealt to a spot [6s Kd] 
UTG+1 : Card dealt to a spot [Ts Ac] 
UTG [ME] : Folds
UTG+1 : Call 30 
UTG+2 : Folds
Dealer : Call 30 
Small Blind : All-in(raise) 470 to 485
Big Blind : Folds
UTG+1 : All-in(raise) 565 to 595
Dealer : Folds
UTG+1 : Return uncalled portion of bet 110 
*** FLOP *** [7h 2c 8h]
*** TURN *** [7h 2c 8h] [9c]
*** RIVER *** [7h 2c 8h 9c] [8d]
Small Blind : Showdown [9s 9h 9c 8h 8d] (Full House)
UTG+1 : Showdown [8h 8d Ac Ts 9c] (One pair)
Small Blind : Hand Result 1030 
*** SUMMARY ***
Total Pot(1030)
Board [7h 2c 8h 9c 8d]
Seat+5: UTG+2 Folded on the FLOP
Seat+2: Dealer Folded on the FLOP
Seat+3: Small Blind 1030  with Full House [9s 9h-9s 9h 9c 8h 8d]  
Seat+4: Big Blind Folded on the FLOP
Seat+1: UTG Folded on the FLOP
Seat+6: UTG+1 lose with One pair [Ts Ac-8h 8d Ac Ts 9c]  


Ignition Hand #3699428132: HOLDEM Tournament #22854969 TBL#1, Turbo- Level 3 (25/50) - 2018-06-26 18:22:11
Seat 5: UTG+1 (505 in chips)
Seat 2: UTG+2 (210 in chips)
Seat 3: Dealer (1,030 in chips)
Seat 4: Small Blind (420 in chips)
Seat 1: Big Blind [ME] (725 in chips)
Seat 6: UTG (110 in chips)
Dealer : Set dealer [3] 
Small Blind : Small blind 25 
Big Blind [ME] : Big blind 50 
*** HOLE CARDS ***
UTG+1 : Card dealt to a spot [6s Qs] 
UTG+2 : Card dealt to a spot [9h Kh] 
Dealer : Card dealt to a spot [8d 5h] 
Small Blind : Card dealt to a spot [Ts Kd] 
Big Blind [ME] : Card dealt to a spot [4c 7h] 
UTG : Card dealt to a spot [5c 9c] 
UTG : Folds
UTG+1 : Folds
UTG+2 : All-in(raise) 210 to 210
Dealer : Folds
Small Blind : Folds
Big Blind [ME] : Folds
UTG+2 : Return uncalled portion of bet 160 
UTG+2 : Does not show [9h Kh] (High Card)
UTG+2 : Hand Result 125 
*** SUMMARY ***
Total Pot(125)
Seat+5: UTG+1 Folded on the FLOP
Seat+2: UTG+2 125 [Does not show]  
Seat+3: Dealer Folded on the FLOP
Seat+4: Small Blind Folded on the FLOP
Seat+1: Big Blind Folded on the FLOP
Seat+6: UTG Folded on the FLOP


Ignition Hand #3699428278: HOLDEM Tournament #22854969 TBL#1, Turbo- Level 3 (25/50) - 2018-06-26 18:22:29
Seat 5: UTG (505 in chips)
Seat 2: UTG+1 (285 in chips)
Seat 3: UTG+2 (1,030 in chips)
Seat 4: Dealer (395 in chips)
Seat 1: Small Blind [ME] (675 in chips)
Seat 6: Big Blind (110 in chips)
Dealer : Set dealer [4] 
Small Blind [ME] : Small blind 25 
Big Blind : Big blind 50 
*** HOLE CARDS ***
UTG : Card dealt to a spot [Jc Ad] 
UTG+1 : Card dealt to a spot [2h Qs] 
UTG+2 : Card dealt to a spot [Kc 3d] 
Dealer : Card dealt to a spot [Ah 7c] 
Small Blind [ME] : Card dealt to a spot [Qd 3c] 
Big Blind : Card dealt to a spot [Td 2d] 
UTG : Raises 125 to 125
UTG+1 : Folds
UTG+2 : Folds
Dealer : Folds
Small Blind [ME] : Folds
Big Blind : All-in 60 
UTG : Return uncalled portion of bet 15 
*** FLOP *** [7h 8h 9s]
*** TURN *** [7h 8h 9s] [6h]
*** RIVER *** [7h 8h 9s 6h] [Tc]
UTG : Showdown [Jc Tc 9s 8h 7h] (Straight)
Big Blind : Showdown [Td 9s 8h 7h 6h] (Straight)
UTG : Hand Result 245 
Big Blind : Ranking 6 
Big Blind : Stand
*** SUMMARY ***
Total Pot(245)
Board [7h 8h 9s 6h Tc]
Seat+5: UTG 245  with Straight [Jc Ad-Jc Tc 9s 8h 7h]  
Seat+2: UTG+1 Folded on the FLOP
Seat+3: UTG+2 Folded on the FLOP
Seat+4: Dealer Folded on the FLOP
Seat+1: Small Blind Folded on the FLOP
Seat+6: Big Blind lose with Straight [Td 2d-Td 9s 8h 7h 6h]  


Ignition Hand #3699428516: HOLDEM Tournament #22854969 TBL#1, Turbo- Level 3 (25/50) - 2018-06-26 18:22:59
Seat 5: Big Blind (640 in chips)
Seat 2: UTG (285 in chips)
Seat 3: UTG+1 (1,030 in chips)
Seat 4: UTG+2 (395 in chips)
Seat 1: Dealer [ME] (650 in chips)
Dealer [ME] : Set dealer [5] 
Big Blind : Big blind 50 
*** HOLE CARDS ***
Big Blind : Card dealt to a spot [7h 9h] 
UTG : Card dealt to a spot [4s Jd] 
UTG+1 : Card dealt to a spot [7s Ah] 
UTG+2 : Card dealt to a spot [9s 2s] 
Dealer [ME] : Card dealt to a spot [3s 6c] 
UTG : Folds
UTG+1 : Sit out
UTG+1 : Folds(timeout)
UTG+2 : Folds
Dealer [ME] : Folds
Big Blind : Return uncalled portion of bet 50 
Big Blind : Does not show [7h 9h] (High Card)
*** SUMMARY ***
Total Pot(0)
Seat+5: Big Blind [Does not show]  
Seat+2: UTG Folded on the FLOP
Seat+3: UTG+1 Folded on the FLOP
Seat+4: UTG+2 Folded on the FLOP
Seat+1: Dealer Folded on the FLOP


Ignition Hand #3699428732: HOLDEM Tournament #22854969 TBL#1, Turbo- Level 3 (25/50) - 2018-06-26 18:23:29
Seat 5: Small Blind (640 in chips)
Seat 2: Big Blind (285 in chips)
Seat 3: UTG (1,030 in chips)
Seat 4: UTG+1 (395 in chips)
Seat 1: UTG+2 [ME] (650 in chips)
Small Blind : Small blind 25 
Big Blind : Big blind 50 
*** HOLE CARDS ***
Small Blind : Card dealt to a spot [Ac 3c] 
Big Blind : Card dealt to a spot [5c 6h] 
UTG : Card dealt to a spot [8s 9c] 
UTG+1 : Card dealt to a spot [Ts 3h] 
UTG+2 [ME] : Card dealt to a spot [Td 8d] 
UTG : Folds
UTG+1 : Folds
UTG+2 [ME] : Folds
Small Blind : Raises 275 to 300
Big Blind : Folds
Small Blind : Return uncalled portion of bet 250 
Small Blind : Does not show [Ac 3c] (High Card)
Small Blind : Hand Result 100 
*** SUMMARY ***
Total Pot(100)
Seat+5: Small Blind 100 [Does not show]  
Seat+2: Big Blind Folded on the FLOP
Seat+3: UTG Folded on the FLOP
Seat+4: UTG+1 Folded on the FLOP
Seat+1: UTG+2 Folded on the FLOP


Ignition Hand #3699428918: HOLDEM Tournament #22854969 TBL#1, Turbo- Level 3 (25/50) - 2018-06-26 18:23:53
Seat 5: Dealer (690 in chips)
Seat 2: Small Blind (235 in chips)
Seat 3: Big Blind (1,030 in chips)
Seat 4: UTG (395 in chips)
Seat 1: UTG+1 [ME] (650 in chips)
Dealer : Set dealer [1] 
Small Blind : Small blind 25 
Big Blind : Big blind 50 
*** HOLE CARDS ***
Dealer : Card dealt to a spot [Jd 7d] 
Small Blind : Card dealt to a spot [4s Jc] 
Big Blind : Card dealt to a spot [6s Ac] 
UTG : Card dealt to a spot [5d Th] 
UTG+1 [ME] : Card dealt to a spot [Ad Ks] 
UTG : Folds
Big Blind : Re-join
UTG+1 [ME] : Raises 132 to 132
Dealer : Folds
Small Blind : Folds
Big Blind : Folds
UTG+1 [ME] : Return uncalled portion of bet 82 
UTG+1 [ME] : Does not show [Ad Ks] (High Card)
UTG+1 [ME] : Hand Result 125 
*** SUMMARY ***
Total Pot(125)
Seat+5: Dealer Folded on the FLOP
Seat+2: Small Blind Folded on the FLOP
Seat+3: Big Blind Folded on the FLOP
Seat+4: UTG Folded on the FLOP
Seat+1: UTG+1 125 [Does not show]  


Ignition Hand #3699429092: HOLDEM Tournament #22854969 TBL#1, Turbo- Level 3 (25/50) - 2018-06-26 18:24:17
Seat 5: UTG+1 (690 in chips)
Seat 2: Dealer (210 in chips)
Seat 3: Small Blind (980 in chips)
Seat 4: Big Blind (395 in chips)
Seat 1: UTG [ME] (725 in chips)
Dealer : Set dealer [2] 
Small Blind : Small blind 25 
Big Blind : Big blind 50 
*** HOLE CARDS ***
UTG+1 : Card dealt to a spot [4h 7d] 
Dealer : Card dealt to a spot [5s Ac] 
Small Blind : Card dealt to a spot [Kh 9h] 
Big Blind : Card dealt to a spot [2d Qh] 
UTG [ME] : Card dealt to a spot [9c Js] 
UTG [ME] : Folds
UTG+1 : Folds
Dealer : All-in(raise) 210 to 210
Small Blind : Folds
Big Blind : Folds
Dealer : Return uncalled portion of bet 160 
Dealer : Does not show [5s Ac] (High Card)
Dealer : Hand Result 125 
*** SUMMARY ***
Total Pot(125)
Seat+5: UTG+1 Folded on the FLOP
Seat+2: Dealer 125 [Does not show]  
Seat+3: Small Blind Folded on the FLOP
Seat+4: Big Blind Folded on the FLOP
Seat+1: UTG Folded on the FLOP


Ignition Hand #3699429285: HOLDEM Tournament #22854969 TBL#1, Turbo- Level 3 (25/50) - 2018-06-26 18:24:44
Seat 5: UTG (690 in chips)
Seat 2: UTG+1 (285 in chips)
Seat 3: Dealer (955 in chips)
Seat 4: Small Blind (345 in chips)
Seat 1: Big Blind [ME] (725 in chips)
Dealer : Set dealer [3] 
Small Blind : Small blind 25 
Big Blind [ME] : Big blind 50 
*** HOLE CARDS ***
UTG : Card dealt to a spot [6h 5h] 
UTG+1 : Card dealt to a spot [6s 3c] 
Dealer : Card dealt to a spot [9d Ks] 
Small Blind : Card dealt to a spot [Kc 9s] 
Big Blind [ME] : Card dealt to a spot [Qd 7c] 
UTG : Raises 125 to 125
UTG+1 : Folds
Dealer : Folds
Small Blind : Folds
Big Blind [ME] : Folds
UTG : Return uncalled portion of bet 75 
UTG : Does not show [6h 5h] (High Card)
UTG : Hand Result 125 
Big Blind [ME] : Sit out
*** SUMMARY ***
Total Pot(125)
Seat+5: UTG 125 [Does not show]  
Seat+2: UTG+1 Folded on the FLOP
Seat+3: Dealer Folded on the FLOP
Seat+4: Small Blind Folded on the FLOP
Seat+1: Big Blind Folded on the FLOP


Ignition Hand #3699429463: HOLDEM Tournament #22854969 TBL#1, Turbo- Level 3 (25/50) - 2018-06-26 18:25:08
Seat 5: Big Blind (765 in chips)
Seat 2: UTG (285 in chips)
Seat 3: UTG+1 (955 in chips)
Seat 4: Dealer (320 in chips)
Seat 1: Small Blind [ME] (675 in chips)
Dealer : Set dealer [4] 
Small Blind [ME] : Small blind 25 
Big Blind : Big blind 50 
*** HOLE CARDS ***
Big Blind : Card dealt to a spot [2s 3d] 
UTG : Card dealt to a spot [5c 6c] 
UTG+1 : Card dealt to a spot [3s 7h] 
Dealer : Card dealt to a spot [9c 4h] 
Small Blind [ME] : Card dealt to a spot [Qd Jc] 
UTG : Folds
Small Blind [ME] : Re-join
UTG+1 : Folds
Dealer : Folds
Small Blind [ME] : Call 25 
Big Blind : Checks
*** FLOP *** [5h As 9s]
Small Blind [ME] : Checks
Big Blind : Checks
*** TURN *** [5h As 9s] [Kh]
Small Blind [ME] : Bets 97 
Big Blind : Folds
Small Blind [ME] : Return uncalled portion of bet 97 
Small Blind [ME] : Does not show [Qd Jc] (High Card)
Small Blind [ME] : Hand Result 100 
*** SUMMARY ***
Total Pot(100)
Board [5h As 9s Kh ]
Seat+5: Big Blind Folded on the TURN
Seat+2: UTG Folded on the FLOP
Seat+3: UTG+1 Folded on the FLOP
Seat+4: Dealer Folded on the FLOP
Seat+1: Small Blind 100 [Does not show]  


Ignition Hand #3699429668: HOLDEM Tournament #22854969 TBL#1, Turbo- Level 4 (50/100) - 2018-06-26 18:25:38
Seat 5: Small Blind (715 in chips)
Seat 2: Big Blind (285 in chips)
Seat 3: UTG (955 in chips)
Seat 4: UTG+1 (320 in chips)
Seat 1: Dealer [ME] (725 in chips)
Dealer [ME] : Set dealer [5] 
Small Blind : Small blind 50 
Big Blind : Big blind 100 
*** HOLE CARDS ***
Small Blind : Card dealt to a spot [Ah 7h] 
Big Blind : Card dealt to a spot [5s 5d] 
UTG : Card dealt to a spot [9s 4h] 
UTG+1 : Card dealt to a spot [8d 6s] 
Dealer [ME] : Card dealt to a spot [3c Ad] 
UTG : Folds
UTG+1 : Folds
Dealer [ME] : All-in(raise) 725 to 725
Small Blind : Folds
Big Blind : All-in 185 
Dealer [ME] : Return uncalled portion of bet 440 
*** FLOP *** [Tc 9c Qh]
*** TURN *** [Tc 9c Qh] [Ac]
*** RIVER *** [Tc 9c Qh Ac] [Kd]
Big Blind : Showdown [5s 5d Ac Kd Qh] (One pair)
Dealer [ME] : Showdown [Ad Ac Kd Qh Tc] (One pair)
Dealer [ME] : Hand Result 620 
Big Blind : Ranking 5 
Big Blind : Stand
*** SUMMARY ***
Total Pot(620)
Board [Tc 9c Qh Ac Kd]
Seat+5: Small Blind Folded on the FLOP
Seat+2: Big Blind lose with One pair [5s 5d-5s 5d Ac Kd Qh]  
Seat+3: UTG Folded on the FLOP
Seat+4: UTG+1 Folded on the FLOP
Seat+1: Dealer 620  with One pair [3c Ad-Ad Ac Kd Qh Tc]  


Ignition Hand #3699429829: HOLDEM Tournament #22854969 TBL#1, Turbo- Level 4 (50/100) - 2018-06-26 18:26:02
Seat 5: Dealer (665 in chips)
Seat 3: Big Blind (955 in chips)
Seat 4: UTG (320 in chips)
Seat 1: UTG+1 [ME] (1,060 in chips)
Dealer : Set dealer [1] 
Big Blind : Big blind 100 
*** HOLE CARDS ***
Dealer : Card dealt to a spot [Ts 4d] 
Big Blind : Card dealt to a spot [Tc 9h] 
UTG : Card dealt to a spot [Qh 6d] 
UTG+1 [ME] : Card dealt to a spot [7s 8d] 
UTG : Folds
UTG+1 [ME] : Folds
Dealer : Folds
Big Blind : Return uncalled portion of bet 100 
Big Blind : Does not show [Tc 9h] (High Card)
*** SUMMARY ***
Total Pot(0)
Seat+5: Dealer Folded on the FLOP
Seat+3: Big Blind [Does not show]  
Seat+4: UTG Folded on the FLOP
Seat+1: UTG+1 Folded on the FLOP


Ignition Hand #3699429910: HOLDEM Tournament #22854969 TBL#1, Turbo- Level 4 (50/100) - 2018-06-26 18:26:14
Seat 5: UTG+1 (665 in chips)
Seat 3: Small Blind (955 in chips)
Seat 4: Big Blind (320 in chips)
Seat 1: UTG [ME] (1,060 in chips)
Small Blind : Small blind 50 
Big Blind : Big blind 100 
*** HOLE CARDS ***
UTG+1 : Card dealt to a spot [4s Js] 
Small Blind : Card dealt to a spot [Ad Jd] 
Big Blind : Card dealt to a spot [3h 5c] 
UTG [ME] : Card dealt to a spot [Kh 2s] 
UTG [ME] : Folds
UTG+1 : Folds
Small Blind : Raises 450 to 500
Big Blind : Folds
Small Blind : Return uncalled portion of bet 400 
Small Blind : Does not show [Ad Jd] (High Card)
Small Blind : Hand Result 200 
*** SUMMARY ***
Total Pot(200)
Seat+5: UTG+1 Folded on the FLOP
Seat+3: Small Blind 200 [Does not show]  
Seat+4: Big Blind Folded on the FLOP
Seat+1: UTG Folded on the FLOP


Ignition Hand #3699430027: HOLDEM Tournament #22854969 TBL#1, Turbo- Level 4 (50/100) - 2018-06-26 18:26:29
Seat 5: UTG (665 in chips)
Seat 3: Dealer (1,055 in chips)
Seat 4: Small Blind (220 in chips)
Seat 1: Big Blind [ME] (1,060 in chips)
Dealer : Set dealer [3] 
Small Blind : Small blind 50 
Big Blind [ME] : Big blind 100 
*** HOLE CARDS ***
UTG : Card dealt to a spot [3s Ad] 
Dealer : Card dealt to a spot [Jh Kd] 
Small Blind : Card dealt to a spot [4c 9d] 
Big Blind [ME] : Card dealt to a spot [Ts 3h] 
UTG : Folds
Dealer : All-in(raise) 1055 to 1055
Small Blind : Folds
Big Blind [ME] : Folds
Dealer : Return uncalled portion of bet 955 
Dealer : Does not show [Jh Kd] (High Card)
Dealer : Hand Result 250 
*** SUMMARY ***
Total Pot(250)
Seat+5: UTG Folded on the FLOP
Seat+3: Dealer 250 [Does not show]  
Seat+4: Small Blind Folded on the FLOP
Seat+1: Big Blind Folded on the FLOP


Ignition Hand #3699430194: HOLDEM Tournament #22854969 TBL#1, Turbo- Level 4 (50/100) - 2018-06-26 18:26:50
Seat 5: Big Blind (665 in chips)
Seat 3: UTG (1,205 in chips)
Seat 4: Dealer (170 in chips)
Seat 1: Small Blind [ME] (960 in chips)
Dealer : Set dealer [4] 
Small Blind [ME] : Small blind 50 
Big Blind : Big blind 100 
*** HOLE CARDS ***
Big Blind : Card dealt to a spot [9h 6h] 
UTG : Card dealt to a spot [Kd 4c] 
Dealer : Card dealt to a spot [Jd 8h] 
Small Blind [ME] : Card dealt to a spot [Tc 8s] 
UTG : Folds
Dealer : All-in(raise) 170 to 170
Small Blind [ME] : Folds
Big Blind : Call 70 
*** FLOP *** [4d 7s 3c]
*** TURN *** [4d 7s 3c] [Qd]
*** RIVER *** [4d 7s 3c Qd] [4s]
Big Blind : Showdown [4s 4d Qd 9h 7s] (One pair)
Dealer : Showdown [4s 4d Qd Jd 8h] (One pair)
Dealer : Hand Result 390 
*** SUMMARY ***
Total Pot(390)
Board [4d 7s 3c Qd 4s]
Seat+5: Big Blind lose with One pair [9h 6h-4s 4d Qd 9h 7s]  
Seat+3: UTG Folded on the FLOP
Seat+4: Dealer 390  with One pair [Jd 8h-4s 4d Qd Jd 8h]  
Seat+1: Small Blind Folded on the FLOP


Ignition Hand #3699430333: HOLDEM Tournament #22854969 TBL#1, Turbo- Level 4 (50/100) - 2018-06-26 18:27:11
Seat 5: Small Blind (495 in chips)
Seat 3: Big Blind (1,205 in chips)
Seat 4: UTG (390 in chips)
Seat 1: Dealer [ME] (910 in chips)
Dealer [ME] : Set dealer [5] 
Small Blind : Small blind 50 
Big Blind : Big blind 100 
*** HOLE CARDS ***
Small Blind : Card dealt to a spot [6d Js] 
Big Blind : Card dealt to a spot [3d Ks] 
UTG : Card dealt to a spot [2d 9d] 
Dealer [ME] : Card dealt to a spot [3h 5s] 
UTG : Folds
Dealer [ME] : Folds
Small Blind : Folds
Big Blind : Does not show [3d Ks] (High Card)
Big Blind : Hand Result 150 
*** SUMMARY ***
Total Pot(150)
Seat+5: Small Blind Folded on the FLOP
Seat+3: Big Blind 150 [Does not show]  
Seat+4: UTG Folded on the FLOP
Seat+1: Dealer Folded on the FLOP


Ignition Hand #3699430415: HOLDEM Tournament #22854969 TBL#1, Turbo- Level 4 (50/100) - 2018-06-26 18:27:23
Seat 5: Dealer (445 in chips)
Seat 3: Small Blind (1,255 in chips)
Seat 4: Big Blind (390 in chips)
Seat 1: UTG [ME] (910 in chips)
Dealer : Set dealer [1] 
Small Blind : Small blind 50 
Big Blind : Big blind 100 
*** HOLE CARDS ***
Dealer : Card dealt to a spot [Ts Qh] 
Small Blind : Card dealt to a spot [8h 2c] 
Big Blind : Card dealt to a spot [Kh Qs] 
UTG [ME] : Card dealt to a spot [Td 4s] 
UTG [ME] : Folds
Dealer : All-in(raise) 445 to 445
Small Blind : Folds
Big Blind : All-in 290 
Dealer : Return uncalled portion of bet 55 
*** FLOP *** [3h 3s 7s]
*** TURN *** [3h 3s 7s] [Ac]
*** RIVER *** [3h 3s 7s Ac] [3c]
Dealer : Showdown [3s 3h 3c Ac Qh] (Three of a kind)
Big Blind : Showdown [3s 3h 3c Ac Kh] (Three of a kind)
Big Blind : Hand Result 830 
*** SUMMARY ***
Total Pot(830)
Board [3h 3s 7s Ac 3c]
Seat+5: Dealer lose with Three of a kind [Ts Qh-3s 3h 3c Ac Qh]  
Seat+3: Small Blind Folded on the FLOP
Seat+4: Big Blind 830  with Three of a kind [Kh Qs-3s 3h 3c Ac Kh]  
Seat+1: UTG Folded on the FLOP


Ignition Hand #3699430686: HOLDEM Tournament #22854969 TBL#1, Turbo- Level 4 (50/100) - 2018-06-26 18:27:59
Seat 5: UTG (55 in chips)
Seat 3: Dealer (1,205 in chips)
Seat 4: Small Blind (830 in chips)
Seat 1: Big Blind [ME] (910 in chips)
Dealer : Set dealer [3] 
Small Blind : Small blind 50 
Big Blind [ME] : Big blind 100 
*** HOLE CARDS ***
UTG : Card dealt to a spot [6s 6d] 
Dealer : Card dealt to a spot [Ah 2d] 
Small Blind : Card dealt to a spot [7s 7d] 
Big Blind [ME] : Card dealt to a spot [9h 7c] 
UTG : All-in 55 
Dealer : Folds
Small Blind : Raises 550 to 600
Big Blind [ME] : Folds
Small Blind : Return uncalled portion of bet 500 
*** FLOP *** [9d Ks Kd]
*** TURN *** [9d Ks Kd] [2s]
*** RIVER *** [9d Ks Kd 2s] [Qc]
UTG : Showdown [Ks Kd 6s 6d Qc] (Two pair)
Small Blind : Showdown [Ks Kd 7s 7d Qc] (Two pair)
Small Blind : Hand Result-Side Pot 90 
Small Blind : Hand Result 165 
UTG : Ranking 4 
UTG : Stand
*** SUMMARY ***
Total Pot(255)
Board [9d Ks Kd 2s Qc]
Seat+5: UTG lose with Two pair [6s 6d-Ks Kd 6s 6d Qc]  
Seat+3: Dealer Folded on the FLOP
Seat+4: Small Blind 255  with Two pair [7s 7d-Ks Kd 7s 7d Qc]  
Seat+1: Big Blind Folded on the FLOP


Ignition Hand #3699430857: HOLDEM Tournament #22854969 TBL#1, Turbo- Level 4 (50/100) - 2018-06-26 18:28:23
Seat 3: Big Blind (1,205 in chips)
Seat 4: Dealer (985 in chips)
Seat 1: Small Blind [ME] (810 in chips)
Dealer : Set dealer [4] 
Small Blind [ME] : Small blind 50 
Big Blind : Big blind 100 
*** HOLE CARDS ***
Big Blind : Card dealt to a spot [4h 8d] 
Dealer : Card dealt to a spot [3d Jc] 
Small Blind [ME] : Card dealt to a spot [9d 6h] 
Dealer : Folds
Small Blind [ME] : Folds
Big Blind : Does not show [4h 8d] (High Card)
Big Blind : Hand Result 150 
*** SUMMARY ***
Total Pot(150)
Seat+3: Big Blind 150 [Does not show]  
Seat+4: Dealer Folded on the FLOP
Seat+1: Small Blind Folded on the FLOP


Ignition Hand #3699430912: HOLDEM Tournament #22854969 TBL#1, Turbo- Level 4 (50/100) - 2018-06-26 18:28:32
Seat 3: Small Blind (1,255 in chips)
Seat 4: Big Blind (985 in chips)
Seat 1: Dealer [ME] (760 in chips)
Dealer [ME] : Set dealer [5] 
Small Blind : Small blind 50 
Big Blind : Big blind 100 
*** HOLE CARDS ***
Small Blind : Card dealt to a spot [Kh Ts] 
Big Blind : Card dealt to a spot [Kc 4h] 
Dealer [ME] : Card dealt to a spot [Qh Qs] 
Dealer [ME] : All-in(raise) 760 to 760
Small Blind : Folds
Big Blind : Folds
Dealer [ME] : Return uncalled portion of bet 660 
Dealer [ME] : Does not show [Qh Qs] (High Card)
Dealer [ME] : Hand Result 250 
*** SUMMARY ***
Total Pot(250)
Seat+3: Small Blind Folded on the FLOP
Seat+4: Big Blind Folded on the FLOP
Seat+1: Dealer 250 [Does not show]  


Ignition Hand #3699431004: HOLDEM Tournament #22854969 TBL#1, Turbo- Level 5 (75/150) - 2018-06-26 18:28:44
Seat 3: Dealer (1,205 in chips)
Seat 4: Small Blind (885 in chips)
Seat 1: Big Blind [ME] (910 in chips)
Dealer : Set dealer [3] 
Small Blind : Small blind 75 
Big Blind [ME] : Big blind 150 
*** HOLE CARDS ***
Dealer : Card dealt to a spot [4s 6c] 
Small Blind : Card dealt to a spot [8d 5h] 
Big Blind [ME] : Card dealt to a spot [8s 7h] 
Dealer : Folds
Small Blind : Folds
Big Blind [ME] : Does not show [8s 7h] (High Card)
Big Blind [ME] : Hand Result 225 
*** SUMMARY ***
Total Pot(225)
Seat+3: Dealer Folded on the FLOP
Seat+4: Small Blind Folded on the FLOP
Seat+1: Big Blind 225 [Does not show]  


Ignition Hand #3699431062: HOLDEM Tournament #22854969 TBL#1, Turbo- Level 5 (75/150) - 2018-06-26 18:28:53
Seat 3: Big Blind (1,205 in chips)
Seat 4: Dealer (810 in chips)
Seat 1: Small Blind [ME] (985 in chips)
Dealer : Set dealer [4] 
Small Blind [ME] : Small blind 75 
Big Blind : Big blind 150 
*** HOLE CARDS ***
Big Blind : Card dealt to a spot [3h 5c] 
Dealer : Card dealt to a spot [Kh Ad] 
Small Blind [ME] : Card dealt to a spot [7s 9h] 
Dealer : All-in(raise) 810 to 810
Small Blind [ME] : Folds
Big Blind : Sit out
Big Blind : Folds(timeout)
Dealer : Return uncalled portion of bet 660 
Dealer : Does not show [Kh Ad] (High Card)
Dealer : Hand Result 375 
Big Blind : Re-join
*** SUMMARY ***
Total Pot(375)
Seat+3: Big Blind Folded on the FLOP
Seat+4: Dealer 375 [Does not show]  
Seat+1: Small Blind Folded on the FLOP


Ignition Hand #3699431269: HOLDEM Tournament #22854969 TBL#1, Turbo- Level 5 (75/150) - 2018-06-26 18:29:23
Seat 3: Small Blind (1,055 in chips)
Seat 4: Big Blind (1,035 in chips)
Seat 1: Dealer [ME] (910 in chips)
Dealer [ME] : Set dealer [5] 
Small Blind : Small blind 75 
Big Blind : Big blind 150 
*** HOLE CARDS ***
Small Blind : Card dealt to a spot [Th 6s] 
Big Blind : Card dealt to a spot [8h Ad] 
Dealer [ME] : Card dealt to a spot [8d Qc] 
Dealer [ME] : Folds
Small Blind : Folds
Big Blind : Does not show [8h Ad] (High Card)
Big Blind : Hand Result 225 
*** SUMMARY ***
Total Pot(225)
Seat+3: Small Blind Folded on the FLOP
Seat+4: Big Blind 225 [Does not show]  
Seat+1: Dealer Folded on the FLOP


Ignition Hand #3699431342: HOLDEM Tournament #22854969 TBL#1, Turbo- Level 5 (75/150) - 2018-06-26 18:29:32
Seat 3: Dealer (980 in chips)
Seat 4: Small Blind (1,110 in chips)
Seat 1: Big Blind [ME] (910 in chips)
Dealer : Set dealer [3] 
Small Blind : Small blind 75 
Big Blind [ME] : Big blind 150 
*** HOLE CARDS ***
Dealer : Card dealt to a spot [5h 5d] 
Small Blind : Card dealt to a spot [8d 4c] 
Big Blind [ME] : Card dealt to a spot [6c Ac] 
Dealer : All-in(raise) 980 to 980
Small Blind : Folds
Big Blind [ME] : All-in 760 
Dealer : Return uncalled portion of bet 70 
*** FLOP *** [Ts 3c As]
*** TURN *** [Ts 3c As] [Td]
*** RIVER *** [Ts 3c As Td] [7c]
Dealer : Showdown [Ts Td 5h 5d As] (Two pair)
Big Blind [ME] : Showdown [As Ac Ts Td 7c] (Two pair)
Big Blind [ME] : Hand Result 1895 
*** SUMMARY ***
Total Pot(1895)
Board [Ts 3c As Td 7c]
Seat+3: Dealer lose with Two pair [5h 5d-Ts Td 5h 5d As]  
Seat+4: Small Blind Folded on the FLOP
Seat+1: Big Blind 1895  with Two pair [6c Ac-As Ac Ts Td 7c]  


Ignition Hand #3699431464: HOLDEM Tournament #22854969 TBL#1, Turbo- Level 5 (75/150) - 2018-06-26 18:29:50
Seat 3: Big Blind (70 in chips)
Seat 4: Dealer (1,035 in chips)
Seat 1: Small Blind [ME] (1,895 in chips)
Dealer : Set dealer [4] 
Small Blind [ME] : Small blind 75 
Big Blind : All-in 70 
*** HOLE CARDS ***
Big Blind : Card dealt to a spot [Jd 5h] 
Dealer : Card dealt to a spot [Kd Js] 
Small Blind [ME] : Card dealt to a spot [7c 3d] 
Dealer : Call 150 
Small Blind [ME] : Call 75 
*** FLOP *** [8h 9s 9c]
Small Blind [ME] : Checks
Dealer : Checks
*** TURN *** [8h 9s 9c] [Ad]
Small Blind [ME] : Checks
Dealer : Checks
*** RIVER *** [8h 9s 9c Ad] [3c]
Small Blind [ME] : Checks
Dealer : Checks
Small Blind [ME] : Showdown [9s 9c 3d 3c Ad] (Two pair)
Big Blind : Mucks [Jd 5h] (One pair)
Dealer : Mucks [Kd Js] (One pair)
Small Blind [ME] : Hand Result-Side Pot 160 
Small Blind [ME] : Hand Result 210 
Big Blind : Ranking 3 
Big Blind : Stand
*** SUMMARY ***
Total Pot(370)
Board [8h 9s 9c Ad 3c]
Seat+3: Big Blind [Mucked] [Jd 5h ]  
Seat+4: Dealer [Mucked] [Kd Js ]  
Seat+1: Small Blind 370  with Two pair [7c 3d-9s 9c 3d 3c Ad]  


Ignition Hand #3699431667: HOLDEM Tournament #22854969 TBL#1, Turbo- Level 5 (75/150) - 2018-06-26 18:30:20
Seat 4: Big Blind (885 in chips)
Seat 1: Dealer [ME] (2,115 in chips)
Dealer [ME] : Set dealer [5] 
Dealer [ME] : Small blind 75 
Big Blind : Big blind 150 
*** HOLE CARDS ***
Big Blind : Card dealt to a spot [2s Ac] 
Dealer [ME] : Card dealt to a spot [Th Jd] 
Dealer [ME] : Raises 1712 to 1787
Big Blind : All-in 735 
Dealer [ME] : Return uncalled portion of bet 902 
*** FLOP *** [8c Tc 6s]
*** TURN *** [8c Tc 6s] [Qh]
*** RIVER *** [8c Tc 6s Qh] [Kh]
Big Blind : Showdown [Ac Kh Qh Tc 8c] (High Card)
Dealer [ME] : Showdown [Th Tc Kh Qh Jd] (One pair)
Dealer [ME] : Hand Result 1770 
Big Blind : Ranking 2 
Big Blind : Prize Cash [$21] 
Dealer [ME] : Ranking 1 
Dealer [ME] : Prize Cash [$39] 
Big Blind : Stand
Dealer [ME] : Stand
*** SUMMARY ***
Total Pot(1770)
Board [8c Tc 6s Qh Kh]
Seat+4: Big Blind lose with High Card [2s Ac-Ac Kh Qh Tc 8c]  
Seat+1: Dealer 1770  with One pair [Th Jd-Th Tc Kh Qh Jd]  

