from django.test import TestCase
from analyzer.parser.main import MainParser
from pathlib import Path

TEST_DIR = Path(__file__).resolve().parent.joinpath("test_files")


class ParserTest(TestCase):
    def test_parse(self):
        pp = MainParser(TEST_DIR)
        pp.parse()
