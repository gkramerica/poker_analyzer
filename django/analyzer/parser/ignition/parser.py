from analyzer.parser.parser_data import Game, Session, Hand, PlayerHand, Action
from analyzer.constants import *
from .parser_utils import *
from analyzer.parser.utils import card_sort_value
from .street_parser import parse_street
import re

ignore = [
    "Seat sit out",
    "Seat re-join",
    "Set dealer",
    "Draw for dealer",
    "Table enter user",
    "Seat sit down",
    "Seat stand",
    "Table leave user",
    "Table deposit",
    "Sit out",
]

TITLE = "TITLE"
TOP = "TOP"


class IgnitionParser:
    def __init__(self, file_name, lines):
        game = Game(file_name)
        self.session = Session(game)
        self.lines = lines

    def split_hands(self):
        all_hands = []
        hand = None
        section = None

        for line in self.lines:
            skip = False

            for ig in ignore:
                if ig in line:
                    skip = True
                    break

            if skip:
                continue

            line = line.strip(" \n")

            if len(line) == 0:
                continue

            if line.startswith("Ignition Hand"):
                hand = {}
                hand[TITLE] = line
                section = TOP
                hand[section] = []
                all_hands.append(hand)
            elif line.startswith("***"):
                section = line[4 : line.index(" ***")]
                hand[section] = []
                if section == "HOLE CARDS":
                    hand[PREFLOP] = []
            elif hand is not None:
                if section == "HOLE CARDS" and not "dealt to a spot" in line:
                    hand[PREFLOP].append(line)
                else:
                    hand[section].append(line)

        return all_hands

    def parse(self):
        game = self.session.game

        self.parse_file_name()

        all_hands = self.split_hands()

        for h in all_hands:
            hand = self.parse_hand(h)
            if hand is not None:
                self.session.hands.append(hand)

        compute_buy_in(game, self.session.hands)
        fix_up_hands(game, self.session.hands)

        return self.session

    def parse_file_name(self):
        game = self.session.game

        if "OMAHA" in game.file_name:
            game.game_type = PLO

        if " MTT " in game.file_name:
            game.game_format = MTT
        elif " STT " in game.file_name:
            game.game_format = STT
        else:
            return

        m = 0
        n = game.file_name.replace("-", " ")
        parts = n.split(" ")
        for p in parts:
            if p.startswith("$"):
                m += float(p[1:])

        game.tournament_buyin = m

    def parse_hand(self, hand_sections):
        hand = self.parse_hand_title(hand_sections[TITLE])
        self.parse_hand_top(hand_sections[TOP], hand)
        self.parse_hole_cards(hand_sections["HOLE CARDS"], hand)
        self.parse_streets(hand_sections, hand)
        self.parse_summary(hand_sections["SUMMARY"], hand)

        return hand

    def parse_hand_title(self, title):
        re_num = r"\s#(.*?)\:"
        re_table = r"TBL#(.*?),"
        re_datetime = r"....\-..\-.....\:..\:.."

        table = find_regex(re_table, title)
        if table is None:
            table = "Zone"

        return Hand(find_regex(re_num, title), table, find_regex(re_datetime, title),)

    def parse_hand_top(self, top, hand):
        for line in top:
            if line.startswith("Seat"):
                p = line.index(":")
                seat = line[4:p].strip()

                name = "Seat" + seat

                if "[ME]" in line:
                    name = HERO
                    line = line.replace("[ME]", "")

                p2 = line.index("(", p + 2)
                pos = line[p + 1 : p2].strip()

                player = hand.add_player(name, pos)
                player.starting_stack = parse_money(line)
            elif "Ante chip" in line:
                p = parse_player(line, hand)
                parts = line.split(" ")
                m = int(parts[len(parts)-1])
                if m > hand.ante:
                    hand.ante = m
                action = Action(p.name, 0, ANTE, m)
                hand.actions.append(action)
            elif line.startswith("Small Blind") or line.startswith("Big Blind"):
                p = parse_player(line, hand)
                m = parse_money(line)
                if line.startswith("Small"):
                    if m > hand.small_blind:
                        hand.small_blind = m
                else:
                    if m > hand.big_blind:
                        hand.big_blind = m

                action = Action(p.name, 0, POST, m)
                hand.actions.append(action)

    def parse_hole_cards(self, hole_cards, hand):
        for line in hole_cards:
            p = parse_player(line, hand)
            if p is not None:
                x = line.replace("[ME]", "")
                pos1 = x.index("[")
                pos2 = x.index("]")

                cards = x[pos1 + 1 : pos2]
                c = cards.split(" ")
                c.sort(key=card_sort_value)
                p.cards = c


    def parse_streets(self, hand_sections, hand):
        for street in STREETS:
            lines = hand_sections.get(street, None)
            if lines is None:
                lines = hand_sections.get(street.upper())

            if lines is None:
                continue

            parse_street(street, lines, hand)

    def parse_summary(self, summary, hand):
        for line in summary:
            b = find_regex(r"Board \[(.*?)\]", line)
            if b is not None:
                hand.boardCards = b.strip().split(" ")
