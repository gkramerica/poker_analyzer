from analyzer.constants import *
from analyzer.push_fold.ranges import RangeHandler
from analyzer.parser.main import MainParser
from analyzer.parser.utils import get_position_name
import sys

class AllinHand:
    def __init__(self, game_num, player_name, hand, pos, pos_name, big_blinds, shove):
        self.hand = hand
        self.game_num = game_num
        self.player_name = player_name
        self.big_blinds = big_blinds
        self.pos = pos
        self.pos_name = pos_name
        self.shove = shove
        self.hand_range = ""
        self.range_pct = 0.0
        self.hand_pct = 0.0

    def __str__(self):
        return f"{self.game_num} {self.hand} {self.player_name} {self.pos_name} {round(self.big_blinds, 1)} {self.shove} {round(self.range_pct, 2)} {round(self.hand_pct, 2)}"

def find_hands(session):
    acts = [CALL, FOLD, RAISE]
    allin_hands = []

    for h in session.hands:
        preflop_actions = filter(lambda x: x.action in acts, h.actions)
        
        if h.big_blind <= 0:
            print(h)
            for a in h.actions:
                print(a)
            continue

        for a in preflop_actions:
            p = h.get_player(a.player)
            if p.pos > -2:
                bbs = p.starting_stack / h.big_blind
                if bbs>=5 and bbs <= 15:
                    raise_pct = a.amount / p.starting_stack
                    shove = a.action == RAISE and raise_pct > .9
                    if not shove and a.action == RAISE:
                        if bbs > 10:
                            continue
                    
                    pos_name = get_position_name(p.pos, len(h.players))
                    ai = AllinHand(h.num, p.name, p.format_hand(), p.pos, pos_name, bbs, shove)
                    allin_hands.append(ai)

                if a.action != FOLD:
                    break

    return allin_hands

class ReportEntry():
    def __init__(self, name):
        self.name = name
        self.all_hands = []
        self.close_hands = []
        self.shoves = []
        self.fold_mistakes = []
        self.push_mistakes = []
        self.worst = 0
        
    def __str__(self):
        tot = len(self.fold_mistakes) + len(self.push_mistakes)
        score = 0
        if len(self.close_hands) > 0:
            score = round((tot / len(self.close_hands))*100)
        return f"{self.name}\t{len(self.all_hands)}\t{len(self.close_hands)}\t{tot}\t{score}%\t{round(self.worst)}"

def score_hands(allin_hands, tolerance=0):
    rh = RangeHandler()

    for h in allin_hands:
        bbs = h.big_blinds
        if bbs > 1 and bbs < 15:
            h.hand_pct = rh.get_hand_percent(h.hand)
            h.hand_range, h.range_pct = rh.get_range(bbs, h.pos)
            
    report = {}

    for h in allin_hands:
        if h.player_name == HERO:
            k = HERO + str(h.pos_name)
        else:
            k = "VILL " + str(h.pos_name)

        if k not in report:
            report[k] = ReportEntry(k)
        
        r = report[k]

        r.all_hands.append(h)

        diff = h.hand_pct - h.range_pct

        if abs(diff) < 25:
            r.close_hands.append(h)

        if abs(diff) <= tolerance:
            continue

        if h.shove:
            r.shoves.append(h)
            if diff > tolerance:
                r.push_mistakes.append(h)
                r.worst = max(abs(diff), r.worst)
        else:
             if diff < tolerance:
                r.fold_mistakes.append(h)
                r.worst = max(abs(diff), r.worst)

    return report
