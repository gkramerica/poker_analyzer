import os
from analyzer.parser.ignition.parser import IgnitionParser


class MainParser:
    def __init__(self, file_path=None):
        if file_path is None:
            self.path = os.environ["HISTORY_DIR"]
        else:
            self.path = file_path

    def parse(self):
        sessions = []
        hands = []
        for f in os.listdir(self.path):
            with open(os.path.join(self.path, f), "r") as reader:
                fp = IgnitionParser(f, reader.readlines())
                session = fp.parse()
                nums = [h.num for h in session.hands]
                dup = []
                for h in session.hands:
                    if h.num in hands:
                        dup.append(h)
                    else:
                        hands.append(h.num)
                
                for d in dup:
                    print("Duplicate " + str(d))
                    session.hands.remove(d)
                sessions.append(session)

        return sessions

