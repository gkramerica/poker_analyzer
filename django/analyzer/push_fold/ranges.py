import os
import pandas as pd
from pathlib import Path
import math


def format_range(hands):
    used = {}

    collpased = []

    for h in hands:
        key = None
        if len(h) == 2:
            key = "pair"
        else:
            key = h[0:1] + h[2:3]

        if not key in used:
            collpased.append(h)
            used[key] = h
        else:
            first = used[key]
            collpased = [x + "+" if x == first else x for x in collpased]

    return ",".join(collpased)

class RangeHandler():
    def __init__(self):
        self.hands = pd.read_csv(Path(__file__).resolve().parent.joinpath("hands.csv"))
        self.ranges = pd.read_csv(Path(__file__).resolve().parent.joinpath("push-fold.csv"))

    def get_percent(self, num_blinds, position):
        if str(position) not in self.ranges.columns:
            return None

        percents = self.ranges[str(position)].to_list()
        if num_blinds >= len(percents):
            return None

        # linear interp between 2 closest blinds
        f = math.floor(num_blinds)
        c = math.ceil(num_blinds)
        if f == c:
            return percents[round(num_blinds) - 1]

        pf = percents[f - 1]
        pc = percents[c - 1]
        
        pct = num_blinds - f
        diff = pc - pf

        x = pf + pct * diff
        return x

    def get_hand_percent(self, hand):
        if len(hand) == 2:
            hand += "o"

        df = self.hands[self.hands["Name"] == hand]    

        if len(df.index) > 0:
            return df["Percent"].to_list()[0]

        return None    

    def get_range(self, num_blinds, position):
        target = self.get_percent(num_blinds, position)
        if target is None:
            return None, None

        df = self.hands[self.hands["Percent"] <= target]

        range_hands = df["Name"].to_list()
        range_hands.reverse()

        return [x[0:2] if x[0:1] == x[1:2] else x for x in range_hands], target
